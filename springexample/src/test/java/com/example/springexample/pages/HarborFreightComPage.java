package com.example.springexample.pages;


import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


public class HarborFreightComPage {

	private static final String PAGE_URL = "www.harborfreight.com";
	WebDriver driver;



	/*@Autowired
	private WebDriver driver;*/
	
	

	public HarborFreightComPage() {
	}

	public void navigate() {
		
		driver.get("https://www.harborfreight.com/");
	}

	
	public WebElement itm63537 = driver.findElement(By.cssSelector("img[alt=\"20V Hypermax™ Lithium 1/2 in. Drill/Driver Kit\"]"));
	
	public WebElement itm63537price = driver.findElement(By.id("product-price-12186"));
	
	
	
	
	public boolean exist(String string) {

		return driver.getPageSource().contains(string);
	}
	
	//Constructor
	public HarborFreightComPage(WebDriver driver){
		this.driver=driver;
		driver.get(PAGE_URL);
		System.out.println("Initialise Elements");
		PageFactory.initElements(driver, this);
	}



}
