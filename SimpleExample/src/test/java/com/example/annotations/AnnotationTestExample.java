package com.example.annotations;

import com.example.annotations.Test;
import com.example.annotations.TesterInfo;
import com.example.annotations.TesterInfo.Priority;

@TesterInfo(
	priority = Priority.HIGH,
	createdBy = "best.tester@adp.com",
	tags = {"Q317","test" }
)
public class AnnotationTestExample {

	@Test
	void testA() {
	  if (true)
		throw new RuntimeException("This test always failed");
	}

	@Test(enabled = false)
	void testB() {
	  if (false)
		throw new RuntimeException("This test always passed");
	}
	
	@Test(enabled = true)
	void testC() {
	  if (10 > 1) {
		// do nothing, this test always passed.
	  }
	}

}