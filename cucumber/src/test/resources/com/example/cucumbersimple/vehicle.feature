@enabled
Feature: Vehicle classification

Scenario: Classify vehicles by the number of tires
Given that the vehicle has "2" tires
When I buy tires 
Then they should be "Motorcycle" tires

Scenario: Classify vehicles by the number of tires
Given that the vehicle has "4" tires
When I buy tires 
Then they should be "Car" tires

Scenario: Classify vehicles by the number of tires
Given that the vehicle has "3" tires
When I buy tires 
Then they should be "Car" tires

Scenario: Classify vehicles by the number of tires
Given that the vehicle has "18" tires
When I buy tires 
Then they should be "truck" tires