package com.example.selenium.pages;

import static org.junit.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class WorkforceLoginPage {
	private WebDriver driver;

	//Page URL
	 
	
	private static String PAGE_URL="https://workforcenow.adp.com/public/index.htm";


	@FindBy(how = How.CSS, using = "input[name='USER']")
	public WebElement username;

	@FindBy(how = How.CSS, using = "input[name='PASSWORD']")
	public WebElement passwd;
	
	@FindBy(how = How.ID, using = "portal.login.logIn")
	public WebElement loginBtn;

	
	public void logIn(String user, String pass) throws InterruptedException {
		String initialUrl = driver.getCurrentUrl();
		username.clear();
		username.sendKeys(user);
		passwd.clear();
		passwd.sendKeys(pass + "\n");
		Thread.sleep(3000);
		//loginBtn.click();
		Thread.sleep(3000);
		if (driver.getCurrentUrl() == initialUrl) {
		System.out.println();
		}
		
	
	}

		
	

	//Constructor
	public WorkforceLoginPage(WebDriver driver){
		this.driver=driver;
		driver.get(PAGE_URL);
		System.out.println("Initialise Elements");
		PageFactory.initElements(driver, this);
	}


}
