package com.example.fundamentals;

public class Conditionals {


	public static void main(String args[]) {
		ifThen();
		ifThenElse();

	}

	private static void ifThen() {

		double curentSpeed = 1.2;
		boolean isMoving = true;

		
		if (isMoving){ 
			// the "then" clause: decrease current speed
			System.out.print("Previous speed " + curentSpeed + "\t\t");
			curentSpeed--;
			System.out.print("New speed " + curentSpeed + "\n");
		}
		
		curentSpeed = 0;
		isMoving = false;

		
		if (isMoving){ 
			// the "then" clause: decrease current speed
			System.out.print("Previous speed " + curentSpeed + "\t\t");
			curentSpeed--;
			System.out.print("New speed " + curentSpeed + "\n");
		}
	}
	
	
		private static void ifThenElse() {

			double curentSpeed = 0;
			boolean isMoving = false;

			
			if (isMoving){ 
				// the "then" clause: decrease current speed
				System.out.print("Previous speed " + curentSpeed + "/t");
				curentSpeed--;
				System.out.print("New speed " + curentSpeed + "\n");

			} else {
				System.out.println("The bike has already stopped");
			}
		
		


	}
}
