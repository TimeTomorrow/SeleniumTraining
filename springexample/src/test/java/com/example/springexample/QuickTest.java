package com.example.springexample;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.example.springexample.pages.GoogleComPage;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:com/example/springexample/automated-chrome-test.xml")
public class QuickTest {
	@Autowired
	private WebDriver driver;

	@Autowired
	private GoogleComPage google;

	@Before
	public void initialize() throws MalformedURLException{
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
	}

	@Test
	public void searchoGoogleTest() {
		google.navigate();
		String firstResult = google.iFeelLuckyFor("Cheese!");

		assertEquals("https://en.wikipedia.org/wiki/Cheese", firstResult);
		

	}

}

