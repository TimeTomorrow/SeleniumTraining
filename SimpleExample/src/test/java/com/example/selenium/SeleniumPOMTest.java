package com.example.selenium;

import org.junit.*;
import org.junit.Assert.*;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.Select;

import com.example.selenium.pages.CalculatorsoupCalculatorsMathBasicPOM;

public class SeleniumPOMTest {


	static WebDriver driver;
	CalculatorsoupCalculatorsMathBasicPOM calculatorPage;
	String baseUrl;
	FirefoxProfile ffprofile;
	FirefoxOptions ffopts;
	DesiredCapabilities ffcaps;


	@Before
	public void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver", "C://Selenium//chromedriver.exe");
		driver = new ChromeDriver();
		baseUrl = "https://www.theonlinecalculator.com/";
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		calculatorPage = new CalculatorsoupCalculatorsMathBasicPOM(driver);
	}

	@Test
	public void testSelIde() throws Exception {		
		calculatorPage.six.click();
		calculatorPage.multiply.click();
		calculatorPage.seven.click();
		
//		calculatorPage.six
		calculatorPage.calculate.click();
		Assert.assertEquals("42", calculatorPage.getDisplayText());	

	}

	@After
	public void tearDown() throws Exception {
		driver.quit(); //close all windows and destroy webdriver instance
	}
}
