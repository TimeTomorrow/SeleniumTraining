package com.example.moreselenium;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

public class jsutils {

	public static void jsscroll(WebDriver driver, int scrollvalue) {
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0," + scrollvalue + ")", "");
	}
}