@enabled
Feature: Basic Math

  Scenario:   Addition of two numbers
    Given The number "1"
    When "1" is added to the total
    Then The result is "2"

  Scenario:   Addition of two numbers incorrectly
    Given The number "2"
    When "3" is added to the total
    Then The result is "6"
    
  Scenario:   Subtraction of three numbers
    Given The number "12"
    When "4" is subtracted to the total
    And "1" is subtracted to the total
    Then The result is "7"
    