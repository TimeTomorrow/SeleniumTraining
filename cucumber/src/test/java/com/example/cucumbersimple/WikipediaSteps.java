package com.example.cucumbersimple;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


//import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.*;


public class WikipediaSteps {

	private WebDriver driver;

	@Before("@web")
	public void before() {
		System.setProperty("webdriver.chrome.driver", "C://Selenium//chromedriver.exe");
		driver = new ChromeDriver();
		assertTrue(true);
	}

	@After("@web")
	public void after() {
		driver.quit();	
		assertEquals(true,false);

	}

	@Given("^Enter search term '(.*?)'$")
	public void searchFor(String searchTerm) {
		WebElement searchField = driver.findElement(By.id("searchInput"));
		searchField.sendKeys(searchTerm);
	}

	@When("^Do search$")
	public void clickSearchButton() {
		WebElement searchButton = driver.findElement(By.id("searchButton"));
		searchButton.click();
	}

	@Then("^Single result is shown for '(.*?)'$")
	public void assertSingleResult(String searchResult) {
		WebElement results = driver.findElement(By.cssSelector("div#mw-content-text.mw-content-ltr p"));
		System.out.println("results are:" + results.getText());
		assertFalse(results.getText().contains(searchResult + " may refer to:"));
		assertTrue(results.getText().startsWith(searchResult));


	}
	
	@Given("^Open http://en\\.wikipedia\\.org$")
	public void open_http_en_wikipedia_org() throws Throwable {
		driver.navigate().to("http://en.wikipedia.org");
	    
	}

	@Given("^Do login$")
	public void do_login() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    
	}
	
}