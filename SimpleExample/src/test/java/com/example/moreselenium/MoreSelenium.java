package com.example.moreselenium;


import java.util.concurrent.TimeUnit;
import org.junit.*;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.example.moreselenium.Screenshot;

public class MoreSelenium {

	private WebDriver driver;

	@After
	public void tearDown() throws Exception {
		driver.close();
	}

	@Before
	public void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver", "C://Selenium//chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@Test
	public void testScreenshot() throws Exception {
		driver.get("https://www.google.com/");
		Screenshot.takescreenshot(driver);
	}
	
	@Test
	public void jsexample() throws InterruptedException {
		driver.get("https://www.amazon.com/");
		jsutils.jsscroll(driver, 200);
		Thread.sleep(3000);
		jsutils.jsscroll(driver, 200);
		Thread.sleep(3000);
		jsutils.jsscroll(driver, -200);
		Thread.sleep(3000);
		jsutils.jsscroll(driver, -200);
		Thread.sleep(3000);
	}
	
}

