#enabled
@web
Feature: search Wikipedia
 
  Background:
    Given Open http://en.wikipedia.org

 
  Scenario: direct search article
    Given Enter search term 'Cucumber'
    When Do search
    Then Single result is shown for 'Cucumber'
    
  Scenario: direct search article
    Given Enter search term 'GNR'
    When Do search
    Then Single result is shown for 'Guns N' Roses'
    
  Scenario: direct search article
    Given Enter search term 'GNR'
    When Do search
    Then Single result is shown for 'Bruno Mars'
    