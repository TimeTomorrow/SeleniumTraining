package com.example.selenium.pages;

import static org.junit.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class WorkforceHomePage {
	private WebDriver driver;

	//Page URL
	 
	
	private static String PAGE_URL="https://workforcenow.adp.com/public/index.htm";



	
	
	@FindBy(how = How.ID, using = "Myself_navItem_label")
	public WebElement myselfMnu;
	
	@FindBy(how = How.CSS, using = "#Myself_ttd_MyselfTabPersonalInformationCategoryProfile > span.revitMegaItemText")
	public WebElement personalProfileMnu;

	
	

		
	

	//Constructor
	public WorkforceHomePage(WebDriver driver){
		this.driver=driver;
		//driver.get(PAGE_URL);
		System.out.println("Initialise Elements");
		PageFactory.initElements(driver, this);
	}


}
