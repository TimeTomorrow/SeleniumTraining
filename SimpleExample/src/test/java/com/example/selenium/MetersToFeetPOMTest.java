package com.example.selenium;

import org.junit.*;
import org.junit.Assert.*;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.Select;

import com.example.selenium.pages.CalculatorsoupCalculatorsMathBasicPOM;
import com.example.selenium.pages.MetersToFeetPOM;

public class MetersToFeetPOMTest {


	static WebDriver driver;
	MetersToFeetPOM metersToFeet;
	String baseUrl;
	FirefoxProfile ffprofile;
	FirefoxOptions ffopts;
	DesiredCapabilities ffcaps;


	@Before
	public void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver", "C://Selenium//chromedriver.exe");
		driver = new ChromeDriver();
		baseUrl = "http://www.meters-to-feet.com/";
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		metersToFeet = new MetersToFeetPOM(driver);
		
	/*	Select select = new Select(driver.findElement(By.xpath("//path_to_drop_down")));
		select.deselectAll();
		select.selectByVisibleText("Value1");*/
		
	}

	@Test
	public void testSelIde() throws Exception {	
		metersToFeet.txtfromvalue.clear();
		metersToFeet.txtfromvalue.sendKeys("5");
		Thread.sleep(5000);
//		assertEquals(metersToFeet.gettxttovalue(),"16.40419948");		

	}

	@After
	public void tearDown() throws Exception {
		driver.quit(); //close all windows and destroy webdriver instance
	}
}
