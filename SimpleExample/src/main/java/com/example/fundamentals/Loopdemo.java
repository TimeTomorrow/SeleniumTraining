package com.example.fundamentals;

public class Loopdemo {

	//public static void main is the default method to run
	public static void main(String args[]) {
		forLoop();
		whileLoop();




	}

	private static void whileLoop() {
		
		//This loop will only run at all if the condition is true
		int count = 1;
		while (count < 11) {
			System.out.println("Count is: " + count);
			count++;  		//This is the incrementor. count++ is the same thing as 
		}					//count = count + 1
		
		
		//This loop always executes at least once before the conditions are evaluated
		int secondCount = 1;
		do {
			System.out.println("Count is: " + secondCount);
			secondCount++;
		} while (secondCount < 11);


	}

	private static void forLoop() {
		int [] numbers = {10, 20, 30, 40, 50};
		//for every integer x in array number
		for(int x : numbers ) {
			System.out.print( x );
			System.out.print(",");
		}
		System.out.print("\n");  //   \n is enter. 



		String [] names = {"James", "Larry", "Tom", "Lacy"};
		//for every string name in array names
		for( String name : names ) {
			System.out.print( name );
			System.out.print(",");
		}
	}

}
