package com.example.junit;

import static org.junit.Assert.*;
import org.junit.Test;

public class JunitExampleTest {

	@Test
	public void thisPasses() {
		assertTrue("Easy one to pass", true);
	}
	
	@Test
	public void thisAlsoPasses() {
		assertTrue("This also passes", 5==4+1);
	}
	
	@Test
	public void thisNotSoMuch() {
		fail("destined for failure");
	}

}
