package com.example.testng;

import org.testng.annotations.*;
import static org.testng.AssertJUnit.*;
import org.testng.asserts.*;




public class TestngExampleTest {
  @Test(priority=1)
  public void firstOne() {
	  assertTrue("Easy one to pass", true);
  }
  
  @Test(priority=2)
  public void secondOne() {
	  assertTrue("another win!", true);
  }
  
  @Test(priority=4)
  public void fourthOne() {
	  assertTrue("another win!", true);
  }
  
  @Test(priority=3)
  public void thirdOne() {
	  assertTrue("another win!", true);
  }
  @BeforeClass
  public void beforeClass() {
  }

  @AfterClass
  public void afterClass() {
  }

}
