package com.example.cucumbersimple;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static org.junit.Assert.assertEquals;



public class vehicleSteps {
	private int numberoftires = 0;
	private String tireswebought;

	@Given("^that the vehicle has \"([^\"]*)\" tires$")
	public void that_the_vehicle_has_tires(int tires) throws Throwable {
		numberoftires = tires;
	}

	@When("^I buy tires$")
	public void i_buy_tires() throws Throwable {
		tireswebought = vehicleIdent(numberoftires);
	}

	@Then("^they should be \"([^\"]*)\" tires$")
	public void they_should_be_tires(String tiresWeShouldHave) throws Throwable {
		assert(tireswebought.equals(tiresWeShouldHave));
	}

	public String vehicleIdent(int tires) {
		String vehicle;
		if (tires==2) { 
			vehicle = "Motorcycle";
		} else if (tires==4) {
			vehicle = "Car";
		} else {
			vehicle = "not in database";
		}
		
		return vehicle;
	}
	

}





