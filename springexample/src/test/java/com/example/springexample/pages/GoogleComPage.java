package com.example.springexample.pages;


import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GoogleComPage {

	@Autowired
	private WebDriver driver;

	public GoogleComPage() {
	}

	public void navigate() {
		driver.get("http://www.google.com");
	}

	public String iFeelLuckyFor(String string) {
		WebElement element = driver.findElement(By.name("q"));
		element.sendKeys(string + "\n"); // send also a "\n"
		/*   		    element.submit();*/

		// wait until the google page shows the result
		WebElement myDynamicElement = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.id("resultStats")));

		List<WebElement> findElements = driver.findElements(By.xpath("//*[@id='rso']//h3/a"));

		for (WebElement webElement : findElements)
		{
			System.out.println(webElement.getAttribute("href"));
		}

		return findElements.get(0).getAttribute("href").toString();

	}

	public boolean exist(String string) {

		return driver.getPageSource().contains(string);
	}



}
