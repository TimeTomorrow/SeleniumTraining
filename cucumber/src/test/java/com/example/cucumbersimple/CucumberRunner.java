package com.example.cucumbersimple;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;


@RunWith(Cucumber.class)
@CucumberOptions(tags= "@enabled",plugin = {"pretty", "html:target/cucumber"})//
public class CucumberRunner {
}
