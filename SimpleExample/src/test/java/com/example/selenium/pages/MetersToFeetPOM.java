package com.example.selenium.pages;

import static org.junit.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class MetersToFeetPOM {
	private WebDriver driver;

	//Page URL
	private static String PAGE_URL="http://www.meters-to-feet.com/";


	@FindBy(how = How.ID, using = "txtfromvalue")
	public WebElement txtfromvalue;

	@FindBy(how = How.ID, using = "txttovalue")
	public WebElement txttovalue;

	public String  gettxttovalue(String text, int number){
		try {
			return txttovalue.getAttribute("value").toString();
		} catch (Error e) {
			System.out.println(e);
			return "";
			//loging stuff etc
		}
		
	
	}

	//Constructor
	public MetersToFeetPOM(WebDriver driver){
		this.driver=driver;
		driver.get(PAGE_URL);
		System.out.println("Initialise Elements");
		PageFactory.initElements(driver, this);
	}


}
