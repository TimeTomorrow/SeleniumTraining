package com.example.selenium;

import org.junit.*;
import org.junit.Assert.*;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.Select;

import com.example.selenium.pages.CalculatorsoupCalculatorsMathBasicPOM;
import com.example.selenium.pages.MetersToFeetPOM;
import com.example.selenium.pages.WorkforceHomePage;
import com.example.selenium.pages.WorkforceLoginPage;

public class WorkforceTest {


	static WebDriver driver;
	WorkforceLoginPage loginpage;
	WorkforceHomePage homepage;
	
	String baseUrl;
	


	@Before
	public void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver", "C://Selenium//chromedriver.exe");
		driver = new ChromeDriver();
		baseUrl = "https://workforcenow.adp.com/public/index.htm";
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		loginpage = new WorkforceLoginPage(driver);
		
	/*	Select select = new Select(driver.findElement(By.xpath("//path_to_drop_down")));
		select.deselectAll();
		select.selectByVisibleText("Value1");*/
		
	}

	@Test
	public void testSelIde() throws Exception {	
loginpage.logIn("Teversmgr@wfnqabvt41", "adpadp31");
homepage = new WorkforceHomePage(driver);
homepage.myselfMnu.click();
homepage.personalProfileMnu.click();

    assertEquals("Personal Profilee", driver.findElement(By.cssSelector("span.dijitTitlePaneTextNode")).getText(),"failed because personal profile was incorrect");
 
  };

	

	@After
	public void tearDown() throws Exception {
		//driver.quit(); //close all windows and destroy webdriver instance
	}
}
