package com.example.selenium.pages;

import static org.junit.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class CalculatorsoupCalculatorsMathBasicPOM {
	private WebDriver driver;

	//Page URL
	private static String PAGE_URL="https://www.calculatorsoup.com/calculators/math/basic.php";

	/*	@FindBy(how = How.LINK_TEXT, using = "APPLY AS A DEVELOPER")
	   private WebElement developerApplyButton;*/

	@FindBy(how = How.NAME, using = "memory_clearButton")
	public WebElement memory_clearButton;

	@FindBy(how = How.NAME, using = "memory_plusButton")
	public WebElement memory_plusButton;

	@FindBy(how = How.NAME, using = "memory_minusButton")
	public WebElement memory_minusButton;

	@FindBy(how = How.NAME, using = "memory_recallButton")
	public WebElement memory_recallButton;

	@FindBy(how = How.NAME, using = "clearButton")
	public WebElement clearButton;

	@FindBy(how = How.NAME, using = "root2")
	public WebElement root2;

	@FindBy(how = How.NAME, using = "percentButton")
	public WebElement percentButton;

	@FindBy(how = How.NAME, using = "divide")
	public WebElement divide;

	@FindBy(how = How.NAME, using = "seven")
	public WebElement seven;

	@FindBy(how = How.NAME, using = "eight")
	public WebElement eight;

	@FindBy(how = How.NAME, using = "nine")
	public WebElement nine;

	@FindBy(how = How.NAME, using = "multiply")
	public WebElement multiply;

	@FindBy(how = How.NAME, using = "five")
	public WebElement five;

	@FindBy(how = How.NAME, using = "six")
	public WebElement six;

	@FindBy(how = How.NAME, using = "subtract")
	public WebElement subtract;

	@FindBy(how = How.NAME, using = "one")
	public WebElement one;

	@FindBy(how = How.NAME, using = "two")
	public WebElement two;

	@FindBy(how = How.NAME, using = "three")
	public WebElement three;

	@FindBy(how = How.NAME, using = "add")
	public WebElement add;

	@FindBy(how = How.NAME, using = "zero")
	public WebElement zero;

	@FindBy(how = How.NAME, using = "decimal")
	public WebElement decimal;

	@FindBy(how = How.NAME, using = "negateButton")
	public WebElement negateButton;

	@FindBy(how = How.NAME, using = "calculate")
	public WebElement calculate;

	@FindBy(how = How.ID, using = "display")
	public WebElement display;

	public String getDisplayText (){
		try {
			return display.getAttribute("value").toString();
		} catch (Error e) {
			System.out.println(e);
			return "";
			//loging stuff etc
		}
	}

	//Constructor
	public CalculatorsoupCalculatorsMathBasicPOM(WebDriver driver){
		this.driver=driver;
		driver.get(PAGE_URL);
		System.out.println("Initialise Elements");
		PageFactory.initElements(driver, this);
	}


}
